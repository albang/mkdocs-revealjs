# `mkdocs-revealjs` Plugin

First mkdocs Plugin attempt. Documentation to come

# How-To make it work

```yaml

site_name: My Awesome portal
theme:
  name: material
plugins:
  - mkdocs-revealjs
  - search
extra_javascript:
  - dist/reveal.js
  - plugin/notes/notes.js
  - plugin/markdown/markdown.js
  - plugin/highlight/highlight.js
  - plugin/externals.js
  - plugin/reveal_custom.js

extra_css:
  - dist/reset.css
  - dist/reveal.css
  - dist/theme/black.css
  - custom/custom.css
  - plugin/highlight/monokai.css

```

custom/custom.css:
```css

.presentation {width:1300px;
height:800px}


```

plugin/reveal_custom.js: 

```js



function loadIncludes() {
        return Promise.all(
          [...document.querySelectorAll("include")].map((include) => {
            return fetch(include.getAttribute("src"))
              .then((response) => response.text())
              .then((html) => (include.outerHTML = html))
              .catch(console.err);
          })
        );
      }
      loadIncludes().then(() => {
        Reveal.initialize({
        		hash: true,
				controls: true,
				progress: true,
				history: true,
				  width: 1200,
  height: 1200,
  minScale: 0.1,
  maxScale: 2.0,
				slideNumber: 'c/t',
             embedded: true,
          hash: true,
          plugins: [RevealMarkdown, RevealHighlight, RevealNotes],
        });
      });


```


At the top of your slides: 

```yaml 

---
presentation: ok
hide:
  - toc
  - navigation
--- 

```